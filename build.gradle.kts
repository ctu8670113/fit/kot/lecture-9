plugins {
    kotlin("jvm") version "1.9.23"
    id("org.openjfx.javafxplugin") version "0.0.8"
}

group = "cz.ctu.fit.bi.kot.ui"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    mavenLocal()

}

dependencies {
    implementation("no.tornado:tornadofx:1.7.20")
    implementation("org.openjfx:javafx-base:22.0.1")
    implementation("org.openjfx:javafx-controls:22.0.1")
    implementation("cz.ctu.fit.bi.kot.rational:lecture-8:1.0-SNAPSHOT")
    testImplementation(kotlin("test"))
}
javafx {
    modules("javafx.controls", "javafx.fxml")
}
tasks.test {
    useJUnitPlatform()
}
kotlin {
    jvmToolchain(21)
}
